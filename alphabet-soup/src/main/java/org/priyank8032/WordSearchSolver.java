package org.priyank8032;

import java.util.ArrayList;
import java.util.List;

class WordSearchSolver {
    private final char[][] grid;
    private final int rows;
    private final int cols;

    public WordSearchSolver(char[][] grid) {
        this.grid = grid;
        this.rows = grid.length;
        this.cols = grid[0].length;
    }

    public List<String> findWords(List<String> words) {
        List<String> foundWords = new ArrayList<>();
        for (String word : words) {
            String position = findWord(word);
            if (position != null) {
                foundWords.add(word + " " + position);
            }
        }
        return foundWords;
    }

    private String findWord(String word) {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                for (int dr = -1; dr <= 1; dr++) {
                    for (int dc = -1; dc <= 1; dc++) {
                        if (dr == 0 && dc == 0) continue;
                        String position = searchFromPosition(word, r, c, dr, dc);
                        if (position != null) {
                            return position;
                        }
                    }
                }
            }
        }
        return null;
    }

    private String searchFromPosition(String word, int r, int c, int dr, int dc) {
        int len = word.length();
        for (int i = 0; i < len; i++) {
            int row = r + i * dr;
            int col = c + i * dc;
            if (!isValidLocation(row, col) || grid[row][col] != word.charAt(i)) {
                return null;
            }
        }
        return r + ":" + c + " " + (r + (len - 1) * dr) + ":" + (c + (len - 1) * dc);
    }

    private boolean isValidLocation(int row, int col) {
        return row >= 0 && row < rows && col >= 0 && col < cols;
    }
}

