package org.priyank8032;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordSearch {
    public static void main(String[] args) {
        String filename = "input.txt"; // replace input file with your input file in project parent directory
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String[] size = br.readLine().split("x");
            int rows = Integer.parseInt(size[0]);
            int cols = Integer.parseInt(size[1]);

            char[][] grid = new char[rows][cols];
            for (int i = 0; i < rows; i++) {
                String[] row = br.readLine().split("\\s+");
                for (int j = 0; j < cols; j++) {
                    grid[i][j] = row[j].charAt(0);
                }
            }

            List<String> words = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                words.add(line);
            }

            WordSearchSolver solver = new WordSearchSolver(grid);
            List<String> foundWords = solver.findWords(words);

            for (String word : foundWords) {
                System.out.println(word);
            }
        } catch (IOException e) {
            System.out.println("Error occurred while reading input file");
        }
    }
}
