package org.priyank8032;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WordSearchSolverTest {
    @Test
    public void testFindWords() {
        char[][] grid = {
                {'H', 'A', 'S', 'D', 'F'},
                {'G', 'E', 'Y', 'B', 'H'},
                {'J', 'K', 'L', 'Z', 'X'},
                {'C', 'V', 'B', 'L', 'N'},
                {'G', 'O', 'O', 'D', 'O'}
        };
        WordSearchSolver solver = new WordSearchSolver(grid);
        List<String> words = Arrays.asList("HELLO", "GOOD", "BYE");
        List<String> foundWords = solver.findWords(words);

        assertEquals(3, foundWords.size());
        assertEquals("HELLO 0:0 4:4", foundWords.get(0));
        assertEquals("GOOD 4:0 4:3", foundWords.get(1));
        assertEquals("BYE 1:3 1:1", foundWords.get(2));
    }

    @Test
    public void testFindWords_SomeWordsFound() {
        char[][] grid = {
                {'H', 'A', 'S', 'D', 'F'},
                {'G', 'E', 'Y', 'B', 'H'},
                {'J', 'K', 'L', 'Z', 'X'},
                {'C', 'V', 'B', 'L', 'N'},
                {'G', 'O', 'O', 'D', 'O'}
        };
        WordSearchSolver solver = new WordSearchSolver(grid);
        List<String> words = Arrays.asList("HELLO", "GOOD", "BYEE");
        List<String> foundWords = solver.findWords(words);

        assertEquals(2, foundWords.size());
        assertEquals("HELLO 0:0 4:4", foundWords.get(0));
        assertEquals("GOOD 4:0 4:3", foundWords.get(1));
    }

    @Test
    public void testFindWords_NoWordsFound() {
        char[][] grid = {
                {'A', 'B', 'C'},
                {'D', 'E', 'F'},
                {'G', 'H', 'I'}
        };
        WordSearchSolver solver = new WordSearchSolver(grid);
        List<String> words = Arrays.asList("CAT", "Y", "Z");
        List<String> foundWords = solver.findWords(words);

        assertTrue(foundWords.isEmpty());
    }
}